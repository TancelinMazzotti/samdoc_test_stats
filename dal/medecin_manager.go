package dal

import (
	"samdoc_test_stats/bo"
)

func SelectMedecinByMailAndPassword(mail string, password string) (bo.Medecin, bool) {
	db := GetInstanceDB()
	medecin := bo.Medecin{}
	success := true

	sqlStatement := `SELECT id_medecin, nom, prenom FROM medecin
	WHERE mail=$1 AND password=crypt($2, password);`

	var id_medecin uint32
	var nom string
	var prenom string

	row := db.QueryRow(sqlStatement, mail, password)

	if err := row.Scan(&id_medecin, &nom, &prenom); err != nil {
		success = false
	} else {
		medecin.Id_medecin = id_medecin
		medecin.Nom = nom
		medecin.Prenom = prenom
		medecin.Mail = mail
	}

	return medecin, success
}
