package dal

import (
	"database/sql"
	"samdoc_test_stats/bo"
)

func SelectStatsInfectionMaladie() ([]bo.StatsInfectionMaladie, bool) {
	db := GetInstanceDB()
	list_stats_infection_maladie := make([]bo.StatsInfectionMaladie, 0)
	success := true

	sqlStatement := `SELECT id_maladie, maladie.libelle, maladie.description, 
	(count(*)::decimal * 100 / (select count(*) from historique)) as pourcentage from maladie
	INNER JOIN historique USING (id_maladie)
	GROUP BY id_maladie, maladie.libelle, maladie.description
	ORDER BY pourcentage desc;`

	rows, err := db.Query(sqlStatement)
	if err != nil {
		success = false
		panic(err)
	}
	for rows.Next() {
		var id_maladie uint32
		var libelle string
		var description sql.NullString
		var stats float32

		if err := rows.Scan(&id_maladie, &libelle, &description, &stats); err != nil {
			success = false
			panic(err)
		}
		maladie := bo.Maladie{}
		maladie.Id_maladie = id_maladie
		maladie.Libelle = libelle
		maladie.Description = description.String
		stats_infection_maladie := bo.StatsInfectionMaladie{}
		stats_infection_maladie.Maladie = maladie
		stats_infection_maladie.Stats = stats
		list_stats_infection_maladie = append(list_stats_infection_maladie, stats_infection_maladie)
	}

	return list_stats_infection_maladie, success
}

func SelectStatsAgeMaladie(id_maladie uint32) ([]bo.StatsAgeMaladie, bool) {
	db := GetInstanceDB()
	list_stats_age_maladie := make([]bo.StatsAgeMaladie, 0)
	success := true

	sqlStatement := `SELECT DATE_PART('year', AGE(now(), naissance)) AS age, count(*) AS nombre_infection
	FROM patient
	INNER JOIN historique USING(id_patient)
	WHERE id_maladie=$1
	GROUP BY age
	ORDER BY age DESC;`

	rows, err := db.Query(sqlStatement, id_maladie)
	if err != nil {
		success = false
		panic(err)
	}
	for rows.Next() {
		var age int
		var stats int

		if err := rows.Scan(&age, &stats); err != nil {
			success = false
			panic(err)
		}
		stats_age_maladie := bo.StatsAgeMaladie{}
		stats_age_maladie.Age = age
		stats_age_maladie.Stats = stats
		list_stats_age_maladie = append(list_stats_age_maladie, stats_age_maladie)
	}

	return list_stats_age_maladie, success
}
