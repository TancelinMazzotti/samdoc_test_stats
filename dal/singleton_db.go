package dal

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"sync"

	_ "github.com/lib/pq"
)

type db_info struct {
	Host     string
	Port     int
	User     string
	Password string
	Dbname   string
}

var instance *sql.DB
var once sync.Once

func createSqlDB() *sql.DB {
	jsonFile, err := os.Open("db_config.json")
	if err != nil {
		panic(err)
	}
	fmt.Println("Successfully Opened db_config.json")
	defer jsonFile.Close()

	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		panic(err)
	}
	var info db_info

	json.Unmarshal(byteValue, &info)

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", info.Host, info.Port, info.User, info.Password, info.Dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}
	return db
}

func GetInstanceDB() *sql.DB {
	if instance == nil {
		once.Do(
			func() {
				fmt.Println("Creating Single Instance Database")
				instance = createSqlDB()
			})
	}
	return instance
}
