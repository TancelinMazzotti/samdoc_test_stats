package main

import (
	"samdoc_test_stats/bll"
	"samdoc_test_stats/dal"

	"github.com/gin-gonic/gin"
	ginserver "github.com/go-oauth2/gin-server"
	"gopkg.in/oauth2.v3/manage"
	"gopkg.in/oauth2.v3/models"
	"gopkg.in/oauth2.v3/server"
	"gopkg.in/oauth2.v3/store"
)

func main() {
	db := dal.GetInstanceDB()
	defer db.Close()

	manager := manage.NewDefaultManager()

	// token store
	manager.MustTokenStorage(store.NewFileTokenStore("token.db"))

	// Initialize the oauth2 service
	ginserver.InitServer(manager)
	ginserver.SetAllowGetAccessRequest(true)
	ginserver.SetClientInfoHandler(server.ClientFormHandler)

	clientStore := store.NewClientStore()
	manager.MapClientStorage(clientStore)

	/**********************************/
	g := gin.Default()

	auth := g.Group("/oauth2")
	{
		auth.POST("/token", func(c *gin.Context) {
			client_id := c.PostForm("client_id")
			client_secret := c.PostForm("client_secret")

			_, err := clientStore.GetByID(client_id)
			if err != nil {
				if bll.AuthentificationMedecin(client_id, client_secret) {
					clientStore.Set(client_id, &models.Client{
						ID:     client_id,
						Secret: client_secret,
					})
				}
			}
			ginserver.HandleTokenRequest(c)
		})
	}

	api := g.Group("/private")
	{
		api.Use(ginserver.HandleTokenVerify())
		api.GET("/stats/infections/maladies", bll.GetNbInfectionsByMaladies)
		api.GET("/stats/age/maladie/:id", bll.GetRepartitionAgeMaladie)

	}

	g.Run(":8081")
}
