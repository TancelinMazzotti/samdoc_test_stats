# Dépendance
go get -u github.com/gin-gonic/gin
go get -u github.com/go-oauth2/gin-server
go get -u github.com/dgrijalva/jwt-go
go get -u github.com/tidwall/buntdb


# Configuration
base de donnée: db_config.json
port: 8081


# [POST] /oauth2/token
Description: Route authentification un compte est créé dans les inserts bdd (login: samdoc@gmail.com password: samdoc)
Post form data:
- grant_type = client_credentials
- client_id = samdoc@gmail.com
- client_secret = samdoc
- scope = read

Exemple resultat:
```
{
    "access_token": "AID3OPE-NS-N8F04PQ0MSW",
    "expires_in": 7200,
    "scope": "read",
    "token_type": "Bearer"
}
```


# [GET] /private/stats/infections/maladies
Description: Nombre de cas par maladie en pourcentage

Exemple resultat:
```
[
    {
        "Maladie": {
            "Id_maladie": 7,
            "Libelle": "Le cancer",
            "Description": ""
        },
        "Stats": 3.5
    },
    {
        "Maladie": {
            "Id_maladie": 41,
            "Libelle": "Lupus",
            "Description": ""
        },
        "Stats": 3.5
    },
    {
        "Maladie": {
            "Id_maladie": 51,
            "Libelle": "Rhumatismes",
            "Description": ""
        },
        "Stats": 3
    },
    ...
]
```

# [GET] /private/stats/age/maladie/:id
Description: Répartition par age nombre d'infection pour une maladie
- id -> id de la maladie

Exemple resultat maladie 14:
```
[
    {
        "Age": 67,
        "Stats": 2
    },
    {
        "Age": 65,
        "Stats": 1
    },
    {
        "Age": 18,
        "Stats": 1
    }
]
```
