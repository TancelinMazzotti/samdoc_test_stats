package bll

import (
	"net/http"
	"samdoc_test_stats/dal"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetNbInfectionsByMaladies(c *gin.Context) {
	list_stats_infection_maladie, success := dal.SelectStatsInfectionMaladie()
	if success {
		c.JSON(200, list_stats_infection_maladie)

	} else {
		c.Writer.WriteHeader(http.StatusNoContent)
	}
}

func GetRepartitionAgeMaladie(c *gin.Context) {
	param_id := c.Param("id")
	id_maladie, err := strconv.ParseInt(param_id, 10, 32)
	if err == nil {
		list_stats_age_maladie, success := dal.SelectStatsAgeMaladie(uint32(id_maladie))
		if success {
			c.JSON(200, list_stats_age_maladie)
		} else {
			c.Writer.WriteHeader(http.StatusNoContent)
		}

	}
}
