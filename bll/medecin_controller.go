package bll

import "samdoc_test_stats/dal"

func AuthentificationMedecin(id string, secret string) bool {
	_, result := dal.SelectMedecinByMailAndPassword(id, secret)
	return result
}
